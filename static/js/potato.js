/**
 * Column indices for filtering
 */
const SIGNIFICANCE = 2;
const CATEGORY = 3;
const USERNAME = 4;
const PHASE = 5;

/**
 * Initialize plugins that are used throughout the application. Called once
 * on page load, sometimes called again after doing a modal load for elements
 * that did not exist before the load.
 */
function initPlugins() {
    /**
     * Initialize tooltips for any element with data-tooltip="tooltip"
     */
    $("[data-toggle='tooltip']").tooltip();

    /**
     * Initialize datepicker for any element with class "datepicker"
     */
    $(".datepicker").datepicker({
        autoclose: true,
        todayHighlight: true,
    }).change(function () {
        /**
         * When you change the value of an input via datepicker, it triggers a
         * "change" event, but the jquery-validation plugin only only
         * re-validates on "blur"
         */
        $(this).trigger("blur");
    });;

    /**
     * Initialize jquery-validation for all form elements
     */
    $("form").each(function (idx, element) {
        $(element).validate({
            errorClass: "is-invalid",
            errorPlacement: function(error, element) {
                let parent = $(element).closest(".input-group");
                if (parent.length) {
                    error.insertAfter(parent);
                } else {
                    error.insertAfter(element);
                }
            },
        });
    });
}

initPlugins();

/**
 * Initialize datatables for the issues list
 */
let table = $("#issues").DataTable();

/**
 * Filter the table based on values of a column
 *
 * @param {string} query filter string
 * @param {int} column column index
 */
function doSearch(query, column) {
    let q = "";

    if (query)
        q = "^" + query + "$";

    table.columns(column).search(q, true).draw();
}

/**
 * Event handlers for the issue list filter dropdowns
 */
$("#filter-significance").change(function (event) {
    doSearch($(this).val(), SIGNIFICANCE);
});

$("#filter-category").change(function (event) {
    doSearch($(this).val(), CATEGORY);
});

$("#filter-user").change(function (event) {
    doSearch($(this).val(), USERNAME);
});

$("#filter-phase").change(function (event) {
    doSearch($(this).val(), PHASE);
});

/**
 * Trigger change event on initial page load to filter the table if the dropdown
 * is preset to the current user (in the event that the user is regular user)
 */
$("#filter-user").trigger("change");

/**
 * Event handler for issue list table row click, to emulate "link" functionality
 */
$("#issues tbody").on("click", "tr[data-href]", function () {
    window.location = $(this).data("href");
});

/**
 * Event handler for button click to load html into a modal element and show it,
 * does a request to the url in the data-href attribute, loads it into the
 * element pointed to by the data-target attribute
 */
$("[data-toggle='modal-load']").click(function (event) {
    event.preventDefault();

    let target = $($(this).data("target"));

    target.load($(this).data("href"), function() {
        $(this).modal("show");
        initPlugins();
    });
});

/**
 * Event handler for button (reject save accept close reopen) clicks in the
 * issue form (close class refers to the x buttons on dialogs, not the close
 * issue button). Marks all textareas (description, phase notes) as required if
 * clicking the accept button, removes required rule otherwise. If errors are
 * found on the main form, the form submit or dialog show actions are prevented.
 */
$("#issue-form button:not(.close, .delete)").click(function (event) {
    let textareas_required = $(this).hasClass("accept");

    $("#issue-form textarea").each(function (idx, element) {
        $(element).rules("add", {
            required: textareas_required,
        });
    });

    if (!$("#issue-form").valid()) { // manually trigger validation on the form
        event.preventDefault(); // stops form from submitting (for submit buttons)
        event.stopPropagation(); // stops dialogs from opening
    }
});

/**
 * Event handler for the "edit" link on comment cards
 */
$("a.comment-edit").click(function (event) {
    // link won't go anywhere but this prevents the page from jumping to the top
    event.preventDefault();

    let card = $(this).closest("div.card");
    let text = card.find("textarea");

    // "editing" class causes form/buttons to show and edit link to hide
    card.addClass("editing");

    // focus the text box
    text.focus();
});

/**
 * Event handler for the "cancel" button on comment cards
 */
$("button.comment-edit-cancel").click(function (event) {
    let card = $(this).closest("div.card");

    // hide the form/buttons and restore the edit link
    card.removeClass("editing");
});
