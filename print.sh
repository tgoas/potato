#!/bin/bash

find . \
    -not \( -path ./static/vendor -prune \) \
    -not \( -name "__init__.py" \) \
    -not \( -name "apps.py" \) \
    -not \( -name "manage.py" \) \
    -name "*.py" \
    -o -name "*.html" \
    -o -name "*.js" \
    -o -name "*.css" \
    -o -name "README.md" \
    -o -name "*.sh" \
    -o -name "Pipfile" \
    -o -name ".env.example" \
| xargs enscript \
    -C \
    -T4 \
    -M Letter \
    -fCourier8 \
    -o - \
| ps2pdf - src.pdf
