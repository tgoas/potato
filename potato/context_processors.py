from potato.models import Issue
from potato.phase import Phase


def total_score(request):
    """
    Calculate the total score contribution of all open issues and that of all
    open issues assigned to the current user. Used for rendering the score bar
    on the top of each page
    """

    total = 0
    user = 0

    # select_related generates a query that joins the related tables, so that
    # all the necessary information is retrieved in one instead of many queries
    issues = Issue.objects.select_related("category", "_phase", "significance").exclude(
        _phase=Phase.CLOSED
    )

    for issue in issues:
        if issue.assigned_to_id == request.user.pk:
            user += issue.score
        total += issue.score

    return {"total_score": total, "user_score": user}


def phases(request):
    """
    Make phase constants available to all templates, i.e. the name DRAFT will
    have a value of 0, etc. for all phases
    """

    return Phase.values()
