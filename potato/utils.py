from django.contrib.auth.decorators import login_required, user_passes_test


def admin_only(func=None, login_url="index"):
    """
    View function decorator that only allows access to logged in administrators
    """

    def is_admin(user):
        return user.is_staff and not user.is_superuser

    decorator = user_passes_test(is_admin, redirect_field_name=None, login_url=login_url)

    if func:
        return login_required(decorator(func))

    return login_required(decorator)


def admin_or_superuser_only(func=None, login_url="index"):
    """
    View function decorator that only allows access to logged in administrators
    or superusers
    """

    def not_user(user):
        return user.is_staff or user.is_superuser

    decorator = user_passes_test(not_user, redirect_field_name=None, login_url=login_url)

    if func:
        return login_required(decorator(func))

    return login_required(decorator)


def user_or_admin_only(func=None, login_url="admin-users"):
    """
    View function decorator that only allows access to logged in administrators
    or regular users
    """

    def not_superuser(user):
        return not user.is_superuser

    decorator = user_passes_test(not_superuser, redirect_field_name=None, login_url=login_url)

    if func:
        return login_required(decorator(func))

    return login_required(decorator)


def form_errors(form):
    """
    Helper to format any errors present in form with the display field name and
    the error message, one to a line, in a single string
    """

    errors = []

    for field_name, errorlist in form.errors.items():
        for error in errorlist:
            errors.append("%s: %s" % (form.fields[field_name].label, error))

    return "\n".join(errors)
