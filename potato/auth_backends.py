from django.contrib.auth.backends import ModelBackend

from potato.models import User

class ProxiedModelBackend(ModelBackend):
    """
    This is here so that the HttpRequest objects pulls out an instance of our
    User proxy model instead of the built-in User model
    """

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
