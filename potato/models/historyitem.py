from django.db import models
from django.utils.functional import cached_property

from potato.phase import Phase

from .category import Category
from .significancelevel import SignificanceLevel
from .user import User


class HistoryItem(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name="+")
    issue = models.ForeignKey("Issue", on_delete=models.CASCADE, related_name="history")
    assigned_to = models.ForeignKey(User, on_delete=models.PROTECT, related_name="+", null=True)
    title = models.CharField(max_length=50, null=True)
    significance = models.ForeignKey(
        SignificanceLevel, on_delete=models.PROTECT, related_name="+", null=True
    )
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name="+", null=True)
    phase = models.PositiveSmallIntegerField(choices=Phase.choices(), null=True)
    prev_phase = models.PositiveSmallIntegerField(choices=Phase.choices(), null=True)
    due_date = models.DateField(null=True)

    @cached_property
    def changed(self):
        """
        List of fields that that were changed on an issue, used for display on
        history items card on the edit issue screen
        """

        result = []

        if not self.pk:
            return result

        if self.phase is not None:
            result.append(("Phase", self.get_phase_display()))

        for field in ["title", "assigned_to", "significance", "category", "due_date"]:
            if getattr(self, field) is not None:
                name = self._meta.get_field(field).verbose_name.capitalize()
                value = getattr(self, field)
                result.append((name, value))

        return result
