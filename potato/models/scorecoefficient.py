from django.db import models


class ScoreCoefficient(models.Model):
    phase = models.PositiveSmallIntegerField(primary_key=True)
    base = models.PositiveIntegerField(default=1)
    time = models.PositiveIntegerField(default=1)
    significance = models.PositiveIntegerField(default=1)
    category = models.PositiveIntegerField(default=1)
