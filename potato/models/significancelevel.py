from django.db import models


class SignificanceLevel(models.Model):
    name = models.CharField(max_length=50, unique=True)
    weight = models.PositiveIntegerField()

    def cant_delete(self):
        """
        If there is any reason the significance level cannot be deleted, a message with
        the reason is returned, otherwise it returns None.
        """

        if self.issues.all():
            return "Significance level has issues assigned!"
        if SignificanceLevel.objects.count() <= 1:
            return "Can't delete the last significance level!"
        return None

    def __str__(self):
        return self.name
