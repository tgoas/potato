from datetime import date

from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import pre_save, post_init, post_save
from django.dispatch import receiver
from django.template.defaultfilters import date as dateformat
from django.template.loader import render_to_string
from django.utils import timezone
from django.utils.functional import cached_property

from potato.phase import Phase

from .category import Category
from .scorecoefficient import ScoreCoefficient
from .significancelevel import SignificanceLevel
from .user import User


class Issue(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name="issues")
    title = models.CharField(max_length=50)
    assigned_to = models.ForeignKey(User, on_delete=models.PROTECT, related_name="assigned_issues")
    significance = models.ForeignKey(
        SignificanceLevel, on_delete=models.PROTECT, related_name="issues"
    )
    category = models.ForeignKey(Category, on_delete=models.PROTECT, related_name="issues")
    _phase = models.ForeignKey(
        ScoreCoefficient,
        on_delete=models.PROTECT,
        related_name="+",
        choices=Phase.choices(),
    )
    due_date = models.DateField()
    description = models.TextField(null=True, blank=True)
    draft_notes = models.TextField(null=True, blank=True)
    screening_notes = models.TextField(null=True, blank=True)
    analysis_notes = models.TextField(null=True, blank=True)
    implementation_notes = models.TextField(null=True, blank=True)
    verification_notes = models.TextField(null=True, blank=True)

    @cached_property
    def activity(self):
        """
        All of this issue's associated HistoryItems and Comments, sorted in a
        list by their creation time
        """

        items = [i for i in self.comments.all()] + [i for i in self.history.all()]
        return sorted(items, key=lambda i: i.created_at, reverse=True)

    def can_edit(self, request):
        """
        Flag representing whether the current user has permission to edit this
        issue
        """

        return self.assigned_to == request.user or request.user.is_staff

    @property
    def next_phase(self):
        """
        Display name of the phase that comes after the current phase
        """

        return Phase(self.phase + 1).name.capitalize() if self.phase < Phase.CLOSED else None

    @property
    def phase(self):
        """
        Property getter for integer value of the current phase.
        """

        return self._phase_id

    @phase.setter
    def phase(self, value):
        """
        Property setter for integer value of the current phase.
        """

        setattr(self, "_phase_id", value)

    @property
    def prev_phase(self):
        """
        Display name of the phase that comes before the current phase
        """

        return Phase(self.phase - 1).name.capitalize() if self.phase > Phase.DRAFT else None

    @cached_property
    def recent_history(self):
        """
        Builds a list of phases, historyitems (if one exists), and tooltip
        messages for each phase. Provides the information used in rendering the
        workflow timeline. The resulting data structure looks like:
        [
            (Phase.DRAFT, HistoryItem, "tooltip message for draft"),
            (Phase.SCREENING, None, ""),
            ...
        ]
        """

        result = []
        for phase in Phase:
            history_item = self.history.filter(prev_phase=phase).order_by("-created_at").first()
            message = ""
            if phase == Phase.CLOSED:
                if history_item and self.phase != Phase.CLOSED:
                    message = "The issue was reopened by %s on %s" % (
                        history_item.created_by,
                        dateformat(timezone.localtime(history_item.created_at), "M j, Y, g:iA"),
                    )
            elif phase == self.phase:
                message = "The issue's current phase is %s and its score is %s" % (
                    phase.name.capitalize(),
                    self.score,
                )
            elif history_item:
                if phase < self.phase:
                    message = "The %s was accepted by %s on %s" % (
                        phase.name.capitalize(),
                        history_item.created_by,
                        dateformat(timezone.localtime(history_item.created_at), "M j, Y, g:iA"),
                    )
                elif phase > self.phase:
                    message = "The %s was rejected by %s on %s" % (
                        phase.name.capitalize(),
                        history_item.created_by,
                        dateformat(timezone.localtime(history_item.created_at), "M j, Y, g:iA"),
                    )
            result.append((phase, history_item, message))
        return result

    @cached_property
    def score(self):
        """
        The all import score calculation method: calculates this issue's score
        based on its phase, time overdue, significance level, and category
        """

        # _phase is a foreign key to the ScoreCoefficient record, made available
        # in the same query via select_related
        coefficient = self._phase

        base = coefficient.base
        time = coefficient.time * max(0, (date.today() - self.due_date).days)
        significance = coefficient.significance * self.significance.weight
        category = coefficient.category * self.category.weight

        return base + time + significance + category


@receiver(post_init, sender=Issue)
@receiver(post_save, sender=Issue)
def save_initial_values(**kwargs):
    """
    Signal handler to save the initial values of fields that are relevant to the
    creation of HistoryItems for later comparison whenever a model instance is
    pulled out of the database or saved.
    """

    instance = kwargs.get("instance")
    fields = ["title", "assigned_to_id", "significance_id", "category_id", "phase", "due_date"]
    if instance.pk:
        instance.initial = {i: getattr(instance, i) for i in fields}


@receiver(pre_save, sender=Issue)
def send_email_notification(**kwargs):
    """
    Send an email to the assigned user when saving if the phase was changed.
    Does not actually send an email if DEBUG=True in settings, instead printing
    the contents of the email that would be sent to the console for inspection.
    """

    instance = kwargs.get("instance")

    # don't send email when creating an issue
    if not instance or not instance.pk:
        return

    # don't send email if phase was not changed
    if instance.phase == instance.initial["phase"]:
        return

    # don't send email when closing an issue
    if instance.phase == Phase.CLOSED:
        return

    context = {
        "user": instance.assigned_to,
        "issue": instance,
        "accepted": instance.phase > instance.initial["phase"],
    }

    contents = render_to_string("email.html", context)

    args = {
        "subject": "Issue Updated: %s" % instance.title,
        "message": contents,
        "from_email": "POTATO Notifications",
        "recipient_list": [instance.assigned_to.email],
        "html_message": contents,
    }

    if settings.DEBUG:
        print(f"---NOT ACTUALLY SENDING EMAIL---\n{args}")
    else:
        send_mail(**args)
