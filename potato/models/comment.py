from django.db import models
from django.template.defaultfilters import date as dateformat
from django.utils import timezone
from django.utils.functional import cached_property

from .issue import Issue
from .user import User


class Comment(models.Model):
    created_at = models.DateTimeField(auto_now_add=True, editable=False)
    modified_at = models.DateTimeField(auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.PROTECT, related_name="+")
    issue = models.ForeignKey(Issue, on_delete=models.CASCADE, related_name="comments")
    text = models.TextField()

    @cached_property
    def tooltip(self):
        """
        Text content of the timestamp tooltip for comment cards. The first line
        contains the creation time. If the comment has been edited, a second
        line will contain the time it was edited.
        """

        result = dateformat(timezone.localtime(self.created_at), "M j, Y, g:iA")
        if self.modified_at != self.created_at:
            result += "\n(edited %s)" % dateformat(
                timezone.localtime(self.modified_at), "M j, Y, g:iA"
            )
        return result
