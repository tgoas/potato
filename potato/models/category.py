from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=50, unique=True)
    weight = models.PositiveIntegerField()

    def cant_delete(self):
        """
        If there is any reason the category cannot be deleted, a message with
        the reason is returned, otherwise it returns None.
        """

        if self.issues.all():
            return "Category has issues assigned!"
        if Category.objects.count() <= 1:
            return "Can't delete the last category!"
        return None

    def __str__(self):
        return self.name
