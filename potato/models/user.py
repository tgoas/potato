from django.contrib.auth.models import User as BaseUser

from potato.phase import Phase


class User(BaseUser):
    def cant_delete(self, request):
        """
        If there is any reason the user cannot be deleted, a message with
        the reason is returned, otherwise it returns None.
        """

        if request.user.pk == self.pk:
            return "You can't delete yourself!"
        if self.is_superuser:
            return "Superuser account cannot be deleted!"
        if self.assigned_issues.exclude(_phase=Phase.CLOSED):
            return "User has issues assigned!"
        return None

    class Meta:
        proxy = True
