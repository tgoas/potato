from potato.models import Comment

from .baseform import BaseForm


class CommentForm(BaseForm):
    class Meta:
        model = Comment
        fields = ["text"]
