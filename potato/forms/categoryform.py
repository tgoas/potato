from potato.models import Category

from .baseform import BaseForm


class CategoryForm(BaseForm):
    class Meta:
        model = Category
        fields = ["name", "weight"]
