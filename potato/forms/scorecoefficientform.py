from potato.models import ScoreCoefficient

from .baseform import BaseForm


class ScoreCoefficientForm(BaseForm):
    class Meta:
        model = ScoreCoefficient
        fields = ["base", "time", "significance", "category"]
