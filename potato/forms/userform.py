from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm, UsernameField
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from potato.models import User


class UserForm(BaseUserCreationForm):
    """
    Form for the User proxy model, making use of django's BaseUserCreationForm
    instead of the BaseForm used in all the other forms.
    """

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        instance = kwargs.get("instance")
        super().__init__(*args, **kwargs)

        # some functionality from baseform
        self.label_suffix = ""
        for name in self.fields:
            self.fields[name].widget.attrs["class"] = "form-control"

        # email is not required by default, but here it is
        self.fields["email"].required = True

        if instance and instance.pk:
            if instance.is_superuser:
                # disable username, email, and admin toggle if editing superuser
                self.fields["username"].disabled = True
                self.fields["email"].disabled = True
                self.fields["email"].required = False
                self.fields["is_staff"].disabled = True
                if instance != user:
                    self.fields["password1"].disabled = True
                    self.fields["password2"].disabled = True

            # password is required by default, but here it is not
            # (user can leave fields blank when editing to leave unchanged)
            self.fields["password1"].required = False
            self.fields["password2"].required = False

    class Meta:
        model = User
        fields = ["username", "email", "is_staff"]
        field_classes = {"username": UsernameField}

    def save(self, commit=True):
        """
        bypass the implementation of save in BaseUserCreationForm, which
        requires a password to be supplied with every request
        """

        user = ModelForm.save(self, commit=False)
        if self.cleaned_data["password1"]:
            user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

    def clean_email(self):
        """
        Enforce uniqueness of email addresses
        """

        email = self.cleaned_data["email"]
        if User.objects.exclude(pk=self.instance.pk).filter(email=email):
            raise ValidationError("Email already in use")
        return email
