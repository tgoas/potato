from django.forms import ModelForm
from django.forms.widgets import DateInput, Textarea


class BaseForm(ModelForm):
    """
    Base class for other ModelForms, overrides a few default widget attributes
    throughout, for compatibility and consistency
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # remove the default trailing colon from label tags
        self.label_suffix = ""

        for name in self.fields:
            # add the form-control class to all widgets, for compatibility with bootstrap
            self.fields[name].widget.attrs["class"] = "form-control"

            if isinstance(self.fields[name].widget, DateInput):
                # add datepicker class also to date input widgets, to active datepicker js plugin
                self.fields[name].widget.attrs["class"] += " datepicker"

                # set date format for compatibility with datepicker plugin
                self.fields[name].widget.format = "%m/%d/%Y"

            if isinstance(self.fields[name].widget, Textarea):
                # reduce textarea rows from 4 to 2
                self.fields[name].widget.attrs["rows"] = 2

    class Meta:
        abstract = True
