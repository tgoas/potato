from potato.models import SignificanceLevel

from .baseform import BaseForm


class SignificanceLevelForm(BaseForm):
    class Meta:
        model = SignificanceLevel
        fields = ["name", "weight"]
