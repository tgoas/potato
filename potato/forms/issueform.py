from django.forms import DateField, ModelChoiceField

from potato.models import Issue, User
from potato.phase import Phase

from .baseform import BaseForm


class IssueForm(BaseForm):
    accept_user = ModelChoiceField(
        queryset=User.objects.filter(is_active=True, is_superuser=False),
        required=False,
        label="Assigned User",
    )
    reject_user = ModelChoiceField(
        queryset=User.objects.filter(is_active=True, is_superuser=False),
        required=False,
        label="Assigned User",
    )
    accept_date = DateField(required=False, label="Due Date")
    reject_date = DateField(required=False, label="Due Date")

    PHASE_NOTES = [
        "draft_notes",
        "screening_notes",
        "analysis_notes",
        "implementation_notes",
        "verification_notes",
    ]

    def __init__(self, *args, **kwargs):
        user = kwargs.pop("user")
        issue = kwargs.get("instance")
        super().__init__(*args, **kwargs)

        # action is what button was pushed to submit the form (save, accept, or reject)
        action = self.data.get("action") if self.is_bound else "save"

        # issue cannot be assigned to superusers or inactive(deleted) users
        self.fields["assigned_to"].queryset = User.objects.filter(
            is_active=True, is_superuser=False
        ).order_by("username")

        # things that apply to everyone for existing issues
        if issue and issue.pk:
            for phase, field in zip(Phase, self.PHASE_NOTES):
                # all phase notes prior to current phase always required
                if phase < issue.phase:
                    self.fields[field].required = True

                # all phase notes after current phase always disabled so contents don't
                # get blown away after rejecting because the fields don't get rendered
                if phase > issue.phase:
                    self.fields[field].disabled = True

            # description always required if phase other than draft
            if issue.phase > Phase.DRAFT:
                self.fields["description"].required = True

            # accept user and accept date required if accepting (but not closing) an issue
            if action == "accept" and issue.phase != Phase.VERIFICATION:
                self.fields["accept_user"].required = True
                self.fields["accept_date"].required = True
                # description always required when accepting
                self.fields["description"].required = True
                # phase notes for current phase required when accepting
                self.fields[self.PHASE_NOTES[issue.phase]].required = True

            # reject user and reject date required if rejecting an issue
            if action == "reject":
                self.fields["reject_user"].required = True
                self.fields["reject_date"].required = True

        # creating an issue
        if not issue or not issue.pk:
            if not user.is_staff:
                # disable and don't require assigned_to, as it is not sent from
                # a disabled form field. this is set by the view when creating
                self.fields["assigned_to"].disabled = True
                self.fields["assigned_to"].required = False

        # closed issue or a regular user viewing an issue assigned to someone else
        elif issue.phase == Phase.CLOSED or not (user.is_staff or issue.assigned_to == user):
            # disable everything
            for field in self.fields:
                self.fields[field].disabled = True

            # except for reject user/date for reopening of issue for admins
            if user.is_staff:
                self.fields["reject_user"].disabled = False
                self.fields["reject_date"].disabled = False

        # regular user viewing their own existing issue
        elif not user.is_staff:
            # disable assigned/due date for existing issue for regular users always
            self.fields["assigned_to"].disabled = True
            self.fields["due_date"].disabled = True

            # disable all phase notes except for the current phase
            for phase, field in zip(Phase, self.PHASE_NOTES):
                self.fields[field].disabled = issue.phase != phase

            # only allow title significance category description on drafts
            if issue.phase > Phase.DRAFT:
                self.fields["title"].disabled = True
                self.fields["significance"].disabled = True
                self.fields["category"].disabled = True
                self.fields["description"].disabled = True

            # don't allow users to assign themselves screening or verification when accepting
            if issue.phase + 1 == Phase.SCREENING or issue.phase + 1 == Phase.VERIFICATION:
                self.fields["accept_user"].queryset = self.fields["accept_user"].queryset.exclude(
                    pk=user.pk
                )

            # don't allow users to assign themselves screening or verification when rejecting
            if issue.phase - 1 == Phase.SCREENING or issue.phase - 1 == Phase.VERIFICATION:
                self.fields["reject_user"].queryset = self.fields["reject_user"].queryset.exclude(
                    pk=user.pk
                )

    class Meta:
        model = Issue
        fields = [
            "title",
            "assigned_to",
            "significance",
            "category",
            "due_date",
            "description",
            "draft_notes",
            "screening_notes",
            "analysis_notes",
            "implementation_notes",
            "verification_notes",
        ]
