from django.contrib.auth.views import LoginView, logout_then_login
from django.urls import path

from potato.views import categories, issues, main, scorecoefficients, significancelevels, users

urlpatterns = [
    # main
    path("", main.index, name="index"),
    # auth
    path("login", LoginView.as_view(), name="login"),
    path("logout", logout_then_login, name="logout"),
    # users
    path("admin/users", users.index, name="admin-users"),
    path("admin/users/create", users.create, name="admin-users-create"),
    path("admin/users/<int:user_id>/edit", users.edit, name="admin-users-edit"),
    path("admin/users/<int:user_id>/delete", users.delete, name="admin-users-delete"),
    path("admin/users/<int:user_id>/check", users.check, name="admin-users-check"),
    # categories
    path("admin/categories", categories.index, name="admin-categories"),
    path("admin/categories/create", categories.create, name="admin-categories-create"),
    path("admin/categories/<int:category_id>/edit", categories.edit, name="admin-categories-edit"),
    path("admin/categories/<int:category_id>/delete", categories.delete, name="admin-categories-delete"),
    path("admin/categories/<int:category_id>/check", categories.check, name="admin-categories-check"),
    # significance levels
    path("admin/significance", significancelevels.index, name="admin-significance"),
    path("admin/significance/create", significancelevels.create, name="admin-significance-create"),
    path("admin/significance/<int:significance_id>/edit", significancelevels.edit, name="admin-significance-edit"),
    path("admin/significance/<int:significance_id>/delete", significancelevels.delete, name="admin-significance-delete"),
    path("admin/significance/<int:significance_id>/check", significancelevels.check, name="admin-significance-check"),
    # score coefficients
    path("admin/scoring", scorecoefficients.index, name="admin-scoring"),
    path("admin/scoring/edit", scorecoefficients.edit, name="admin-scoring-edit"),
    # issues
    path("issues/open", issues.index, {'closed': False}, name="issues-open"),
    path("issues/closed", issues.index, {'closed': True}, name="issues-closed"),
    path("issues/create", issues.create, name="issues-create"),
    path("issues/<int:issue_id>", issues.edit, name="issues-edit"),
    path("issues/<int:issue_id>/delete", issues.delete, name="issues-delete"),
    path("issues/<int:issue_id>/comment", issues.comment, name="issues-comment"),
    path("issues/<int:issue_id>/comment/<int:comment_id>", issues.edit_comment, name="issues-comment-edit"),
]
