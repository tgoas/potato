from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages

from potato.forms import UserForm
from potato.models import User
from potato.utils import admin_or_superuser_only, form_errors


@admin_or_superuser_only
def index(request):
    """
    Display the Account Management screen. Users ordered first by type
    (superuser > admin > user), then by name.
    """

    users = User.objects.filter(is_active=True).order_by("-is_superuser", "-is_staff", "username")
    context = {"users": users, "form": UserForm(user=request.user, prefix="add")}

    return render(request, "admin/users/index.html", context)


@admin_or_superuser_only
def create(request):
    """
    Accepts HTTP POST request from the Create New User form on the Account
    Management screen to create a new User.
    """

    if request.method == "POST":
        form = UserForm(request.POST, user=request.user, prefix="add")

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            form.save()
            messages.success(request, "New user successfully saved.")

    return redirect("admin-users")


@admin_or_superuser_only
def edit(request, user_id):
    """
    Renders the contents of the Modify User dialog, and accepts HTTP POST
    request from that dialog to modify a User.
    """

    user = get_object_or_404(User, pk=user_id)

    if request.method == "POST":
        form = UserForm(request.POST, user=request.user, prefix="edit", instance=user)
        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            form.save()
            messages.success(request, "User successfully edited.")

        return redirect("admin-users")

    context = {
        "form": UserForm(prefix="edit", user=request.user, instance=user),
        "cant_delete": user.cant_delete(request),
    }

    return render(request, "admin/users/edit.html", context)


@admin_or_superuser_only
def delete(request, user_id):
    """
    "Delete" a User by id. Checks first to see if the user can be deleted, and
    returns an error message if not. Users are never actually deleted, but set
    as inactive, so that their information is still accessible by history items,
    comments, and issues.
    """

    user = get_object_or_404(User, pk=user_id)

    cant_delete = user.cant_delete(request)

    if cant_delete:
        messages.error(request, cant_delete)
    else:
        user.is_active = False
        user.save()
        messages.success(request, "User successfully deleted.")

    return redirect("admin-users")


@admin_or_superuser_only
def check(request, user_id=None):
    """
    Accepts HTTP GET request sent by jquery validation plugin for remote
    validation. Request contains the id of the field as the key, and the value
    entered by the user for validation as the value.
    """

    user = None
    if user_id:
        user = get_object_or_404(User, pk=user_id)

    # validator only sends one key/value at a time for validation, so a request
    # containing more than one key is an error condition
    if len(request.GET) != 1:
        return HttpResponseBadRequest()

    form = UserForm(
        request.GET, user=request.user, instance=user, prefix="edit" if user_id else "add"
    )

    # since request.GET is a QueryDict, the keys are in an arbitrary non-random
    # order and there is no way to directly "grab the first element," but we are
    # guaranteed by the above check that there is exactly one key in the dict,
    # so iterating the dict is a (roundabout) way of grabbing the element
    # without knowing the name of the key.
    for key in request.GET:
        # key will be one of (depending on the form prefix):
        #   add-username, add-email, add-password2,
        #   edit-username, edit-email, edit-password2
        for field in ["username", "email", "password2"]:
            if field in key:
                return JsonResponse("<br/>".join(form.errors.get(field, [])) or True, safe=False)

    return JsonResponse(False, safe=False)
