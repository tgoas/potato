from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect


@login_required
def index(request):
    """
    Root URL view function. Redirects superuser to Account Management, otherwise
    redirects to Open Issues List
    """

    if request.user.is_superuser:
        return redirect("admin-users")
    return redirect("issues-open")
