from django.shortcuts import redirect, render
from django.contrib import messages

from potato.forms import ScoreCoefficientForm
from potato.models import ScoreCoefficient
from potato.phase import Phase
from potato.utils import admin_only, form_errors


@admin_only
def index(request):
    """
    Display the Score Coefficient Management screen
    """

    forms = []

    # build a list of forms for each phase draft - verification, in order
    for phase in [Phase(i) for i in range(5)]:
        forms.append(
            (
                phase.name.capitalize(),
                ScoreCoefficientForm(
                    instance=ScoreCoefficient.objects.get(phase=phase), prefix=phase.name.lower()
                ),
            )
        )

    context = {"forms": forms}

    return render(request, "admin/scoring/index.html", context)


@admin_only
def edit(request):
    """
    Accepts HTTP POST request from form on Score Coefficient Management screen.
    Checks all 5 forms for errors before saving, if any have errors, none are
    saved and the error messages are returned to the user.
    """

    forms = []

    # build a list of forms for each phase draft - verification, in order
    for phase in [Phase(i) for i in range(5)]:
        forms.append(
            ScoreCoefficientForm(
                request.POST,
                instance=ScoreCoefficient.objects.get(phase=phase),
                prefix=phase.name.lower(),
            )
        )

    errors = []

    for form in forms:
        if not form.is_valid():
            errors.append(form_errors(form))

    if errors:
        messages.error(request, "The following errors were detected:\n%s" % "\n".join(errors))
    else:
        for form in forms:
            form.save()
        messages.success(request, "Score coefficients successfully saved.")

    return redirect("admin-scoring")
