from django.db import IntegrityError
from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages

from potato.forms import SignificanceLevelForm
from potato.models import SignificanceLevel
from potato.utils import admin_only, form_errors


@admin_only
def index(request):
    """
    Display the Significance Level Management screen
    """

    context = {
        "significance_levels": SignificanceLevel.objects.order_by("weight"),
        "form": SignificanceLevelForm(prefix="add"),
    }

    return render(request, "admin/significancelevels/index.html", context)


@admin_only
def create(request):
    """
    Accepts HTTP POST request from the Create New Significance Level form on the
    Significance Level Management screen to create a new SignificanceLevel
    """

    form = SignificanceLevelForm(request.POST, prefix="add")

    if not form.is_valid():
        messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
    else:
        form.save()
        messages.success(request, "New significance level successfully saved.")

    return redirect("admin-significance")


@admin_only
def edit(request, significance_id):
    """
    Renders the contents of the Modify Significance dialog, and accepts HTTP
    POST request from that dialog to modify a SignificanceLevel
    """

    significance = get_object_or_404(SignificanceLevel, pk=significance_id)

    if request.method == "POST":
        form = SignificanceLevelForm(request.POST, prefix="edit", instance=significance)

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            form.save()
            messages.success(request, "Significance level successfully edited.")

        return redirect("admin-significance")

    context = {
        "cant_delete": significance.cant_delete(),
        "form": SignificanceLevelForm(prefix="edit", instance=significance),
    }

    return render(request, "admin/significancelevels/edit.html", context)


@admin_only
def delete(request, significance_id):
    """
    Delete a SignificanceLevel by id. Checks first to see if the significance
    level can be deleted and returns an error message if not.
    """

    significance = get_object_or_404(SignificanceLevel, pk=significance_id)

    cant_delete = significance.cant_delete()

    if cant_delete:
        messages.error(request, cant_delete)
    else:
        try:
            significance.delete()
        except IntegrityError as exception:
            # IntegrityError usually means the violation of a database constraint
            messages.error(request, "Something went wrong deleting the significance level!")
            print(exception)
        else:
            messages.success(request, "Significance level successfully deleted.")

    return redirect("admin-significance")


@admin_only
def check(request, significance_id=None):
    """
    Accepts HTTP GET request sent by jquery validation plugin for remote
    validation. Request contains the id of the field as the key, and the value
    entered by the user for validation as the value.
    """

    significance = None
    if significance_id:
        significance = get_object_or_404(SignificanceLevel, pk=significance_id)

    # validator only sends one key/value at a time for validation, so a request
    # containing more than one key is an error condition
    if len(request.GET) != 1:
        return HttpResponseBadRequest()

    form = SignificanceLevelForm(
        request.GET, instance=significance, prefix="edit" if significance_id else "add"
    )

    # since request.GET is a QueryDict, the keys are in an arbitrary non-random
    # order and there is no way to directly "grab the first element," but we are
    # guaranteed by the above check that there is exactly one key in the dict,
    # so iterating the dict is a (roundabout) way of grabbing the element
    # without knowing the name of the key.
    for key in request.GET:
        # key will be either add-name or edit-name, depending on the form prefix
        if "name" in key:
            return JsonResponse("<br/>".join(form.errors.get("name", [])) or True, safe=False)

    return JsonResponse(False, safe=False)
