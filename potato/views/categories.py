from django.db import IntegrityError
from django.http import HttpResponseBadRequest, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages

from potato.forms import CategoryForm
from potato.models import Category
from potato.utils import admin_only, form_errors


@admin_only
def index(request):
    """
    Display the Category Management screen
    """

    context = {
        "categories": Category.objects.order_by("weight"),
        "form": CategoryForm(prefix="add"),
    }

    return render(request, "admin/categories/index.html", context)


@admin_only
def create(request):
    """
    Accepts HTTP POST request from the Create New Category form on the Category
    Management screen to create a new Category
    """

    if request.method == "POST":
        form = CategoryForm(request.POST, prefix="add")

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            form.save()
            messages.success(request, "New category successfully saved.")

    return redirect("admin-categories")


@admin_only
def edit(request, category_id):
    """
    Renders the contents of the Modify Category dialog, and accepts HTTP POST
    request from that dialog to modify a Category
    """

    category = get_object_or_404(Category, pk=category_id)

    if request.method == "POST":
        form = CategoryForm(request.POST, prefix="edit", instance=category)

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            form.save()
            messages.success(request, "Category successfully edited.")

        return redirect("admin-categories")

    context = {
        "cant_delete": category.cant_delete(),
        "form": CategoryForm(prefix="edit", instance=category),
    }

    return render(request, "admin/categories/edit.html", context)


@admin_only
def delete(request, category_id):
    """
    Delete a Category by id. Checks first to see if the category can be deleted
    and returns an error message if not.
    """

    category = get_object_or_404(Category, pk=category_id)

    cant_delete = category.cant_delete()

    if cant_delete:
        messages.error(request, cant_delete)
    else:
        try:
            category.delete()
        except IntegrityError as exception:
            # IntegrityError usually means the violation of a database constraint
            messages.error(request, "Something went wrong deleting the category!")
            print(exception)
        else:
            messages.success(request, "Category successfully deleted.")

    return redirect("admin-categories")


@admin_only
def check(request, category_id=None):
    """
    Accepts HTTP GET request sent by jquery validation plugin for remote
    validation. Request contains the id of the field as the key, and the value
    entered by the user for validation as the value.
    """

    category = None
    if category_id:
        category = get_object_or_404(Category, pk=category_id)

    # validator only sends one key/value at a time for validation, so a request
    # containing more than one key is an error condition
    if len(request.GET) != 1:
        return HttpResponseBadRequest()

    form = CategoryForm(request.GET, instance=category, prefix="edit" if category_id else "add")

    # since request.GET is a QueryDict, the keys are in an arbitrary non-random
    # order and there is no way to directly "grab the first element," but we are
    # guaranteed by the above check that there is exactly one key in the dict,
    # so iterating the dict is a (roundabout) way of grabbing the element
    # without knowing the name of the key.
    for key in request.GET:
        # key will be either add-name or edit-name, depending on the form prefix
        if "name" in key:
            return JsonResponse("<br/>".join(form.errors.get("name", [])) or True, safe=False)

    return JsonResponse(False, safe=False)
