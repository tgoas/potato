from datetime import date, timedelta

from django.db import IntegrityError, transaction
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.contrib import messages

from potato.forms import CommentForm, IssueForm
from potato.models import Category, Comment, HistoryItem, Issue, SignificanceLevel, User
from potato.phase import Phase
from potato.utils import admin_only, form_errors, user_or_admin_only


@user_or_admin_only
def index(request, closed=False):
    """
    Display the Open/Closed Issues List screen
    """

    # select_related generates a query that joins the related tables, so that
    # all the necessary information is retrieved in one instead of many queries
    issues = Issue.objects.select_related("assigned_to", "category", "_phase", "significance")

    # if requesting the closed issue list
    if closed:
        issues = issues.filter(_phase=Phase.CLOSED)
        users = User.objects.none()
    else:
        issues = issues.exclude(_phase=Phase.CLOSED)
        users = User.objects.filter(is_active=True, is_superuser=False).order_by("username")

    context = {
        "closed": closed,
        "significance_levels": SignificanceLevel.objects.order_by("name"),
        "categories": Category.objects.order_by("name"),
        "issues": issues,
        "users": users,
    }

    return render(request, "issues/list/index.html", context)


@user_or_admin_only
def create(request):
    """
    Display the Create New Issue screen, and accepts HTTP POST request from the
    form on that screen to create a new Issue
    """

    # mock issue that will not be saved in the database, to supply some defaults
    issue = Issue(
        # default to assigned to current user
        assigned_to=request.user,
        # causes timeline to render all empty
        phase=-1,
        # default to one week from today
        due_date=date.today() + timedelta(weeks=1),
    )

    form = IssueForm(user=request.user, instance=issue)

    if request.method == "POST":
        form = IssueForm(request.POST, user=request.user)

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            issue = form.save(commit=False)
            issue.phase = Phase.DRAFT
            issue.created_by = request.user

            # if the Issue returned from the form save does not have an assigned
            # user, it is because that control was disabled for whoever
            # submitted the form. in this case we assign the issue to the
            # current user.
            if not hasattr(issue, "assigned_to"):
                issue.assigned_to = request.user

            try:
                # using an atomic transaction to guarantee both inserts succeed
                # or both fail, to prevent inconsistent state if one fails
                with transaction.atomic():
                    issue.save()
                    HistoryItem.objects.create(
                        phase=Phase.DRAFT, issue=issue, created_by=request.user
                    )
            except IntegrityError as exception:
                # IntegrityError usually means the violation of a database constraint
                messages.error(request, "Something went wrong saving the issue!")
                print(exception)
            else:
                messages.success(request, "New issue successfully saved.")

            return redirect("issues-edit", issue.pk)

    context = {"can_edit": False, "issue": issue, "issue_form": form}

    return render(request, "issues/edit/index.html", context)


@user_or_admin_only
def edit(request, issue_id):
    """
    Display the Edit/View Issue screen, and accepts HTTP POST request from the
    form on that screen to update the Issue.
    """

    try:
        # select_related generates a query that joins the related tables, so that
        # all the necessary information is retrieved in one instead of many queries
        #
        # prefetch_related generates one additional query foe each relationship
        # with the necessary joins to retrieve many-valued related items in a
        # single query, instead of retrieving the records one at a time
        issue = (
            Issue.objects.select_related("assigned_to", "category", "_phase", "significance")
            .prefetch_related(
                "comments__created_by",
                "history__assigned_to",
                "history__category",
                "history__created_by",
                "history__significance",
            )
            .get(pk=issue_id)
        )
    except Issue.DoesNotExist:
        raise Http404

    form = IssueForm(user=request.user, instance=issue)

    if request.method == "POST":
        # action is what button was pushed to submit the form (save, accept, or reject)
        action = request.POST.get("action")

        form = IssueForm(request.POST, user=request.user, instance=issue)

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            updated_issue = form.save(commit=False)

            if action == "accept":
                # update the assignee and due date when accepting but not
                # closing an issue (these fields are hidden when closing)
                if updated_issue.phase != Phase.VERIFICATION:
                    updated_issue.assigned_to = form.cleaned_data["accept_user"]
                    updated_issue.due_date = form.cleaned_data["accept_date"]

                # increment the phase when accepting
                updated_issue.phase = updated_issue.phase + 1

            if action == "reject":
                # update the assignee and due date when rejecting always
                updated_issue.assigned_to = form.cleaned_data["reject_user"]
                updated_issue.due_date = form.cleaned_data["reject_date"]

                # decrement the phase when rejecting
                updated_issue.phase = updated_issue.phase - 1

            # flag determines whether to create a new history item
            changed = False
            history_item = HistoryItem()

            # compare the values saved by the save initial values signal handler
            # to the current values, and record any changes on the history item
            for field in updated_issue.initial:
                if getattr(updated_issue, field) != updated_issue.initial[field]:
                    setattr(history_item, field, getattr(updated_issue, field))
                    if field == "phase":
                        setattr(history_item, "prev_phase", updated_issue.initial[field])
                    changed = True

            try:
                # using an atomic transaction to guarantee both the insert and
                # update succeed or they both fail, to prevent inconsistent
                # state if one fails
                with transaction.atomic():
                    if changed:
                        history_item.issue = updated_issue
                        history_item.created_by = request.user
                        history_item.save()
                    updated_issue.save()
            except IntegrityError as exception:
                # IntegrityError usually means the violation of a database constraint
                messages.error(request, "Something went wrong saving the issue!")
                print(exception)
            else:
                messages.success(request, "Issue successfully updated.")

            return redirect("issues-edit", issue.pk)

    context = {
        "can_edit": issue.can_edit(request),
        "issue": issue,
        "issue_form": form,
        "comment_form": CommentForm(prefix="comment"),
    }

    return render(request, "issues/edit/index.html", context)


@admin_only
def delete(request, issue_id):
    """
    Delete an Issue by id. Checks first to see if the issue can be deleted
    (phase is closed), and returns an error message if not.
    """

    issue = get_object_or_404(Issue, pk=issue_id)

    if request.method == "POST":
        if issue.phase != Phase.CLOSED:
            messages.error(request, "Only closed issues can be deleted!")
            return redirect("issues-edit", issue_id)

        try:
            issue.delete()
        except IntegrityError as exception:
            # IntegrityError usually means the violation of a database constraint
            messages.error(request, "Something went wrong deleting the issue!")
            print(exception)
            return redirect("issues-edit", issue_id)
        else:
            messages.success(request, "Issue successfully deleted.")

    return redirect("issues-open")


@user_or_admin_only
def comment(request, issue_id):
    """
    Accepts HTTP POST request from Add Comment form on Edit Issue page. Creates
    a new Comment and associates it to the issue using the provided id.
    """

    issue = get_object_or_404(Issue, pk=issue_id)

    if request.method == "POST":
        form = CommentForm(request.POST, prefix="comment")

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            new_comment = form.save(commit=False)
            new_comment.issue = issue
            new_comment.created_by = request.user
            new_comment.save()
            messages.success(request, "New comment successfully saved.")

    return redirect("issues-edit", issue.pk)


@admin_only
def edit_comment(request, issue_id, comment_id):
    """
    Accepts HTTP POST request from Edit Comment form on Edit Issue page. Updates
    the Comment text using the comment id, then redirects back to Edit Issue
    using the issue id.
    """

    issue = get_object_or_404(Issue, pk=issue_id)
    instance = get_object_or_404(Comment, pk=comment_id)

    if request.method == "POST":
        form = CommentForm(request.POST, instance=instance)

        if not form.is_valid():
            messages.error(request, "The following errors were detected:\n%s" % form_errors(form))
        else:
            form.save()
            messages.success(request, "Comment successfully edited.")

    return redirect("issues-edit", issue.pk)
