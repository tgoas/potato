from enum import IntEnum


class Phase(IntEnum):
    """
    The Phase class defines a way to represent issue phases as integer values,
    while maintaining readibility of a string and the database performance of
    an integer
    """

    DRAFT = 0
    SCREENING = 1
    ANALYSIS = 2
    IMPLEMENTATION = 3
    VERIFICATION = 4
    CLOSED = 5

    @classmethod
    def choices(cls):
        """
        Choice values (int value, display name) used to restrict model fields to
        valid phases and for mapping integer values to display names
        """

        return [(i.value, i.name.capitalize()) for i in cls]

    @classmethod
    def values(cls):
        """
        Dictionary mapping names to their integer value, for making the phase
        constants available in the template layer
        """

        return {i.name: i.value for i in cls}
