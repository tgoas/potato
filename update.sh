#!/bin/bash

# run these commands as www-data
echo "doing update..."
sudo -H -u www-data bash -c '\
    cd /srv/potato && \
    source env/bin/activate && \
    git pull && \
    python manage.py migrate && \
    python manage.py collectstatic --noinput'

# run this command as root
echo "restarting apache..."
sudo service apache2 restart
echo "done"
