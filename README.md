# POTATO
Priority Optimized Tool for Action Tracking Online

# Development Setup

## Install Python 3.6.x, pip and pipenv

Guide: https://pipenv.readthedocs.io/en/latest/install/

> Note if you use vscode and are thinking about using WSL that vscode's python extension does not currently play nice with WSL.

## Clone this repository

[Add an SSH key](https://docs.gitlab.com/ee/ssh/README.html) to your gitlab profile if you haven't already

Change into an empty directory

```
git clone git@gitlab.com:tgoas/potato.git .
```

## Install dependencies

This will create a virtualenv and install the necessary external dependencies

```
pipenv install --dev
```

## Configuration

#### Environment variables

Copy `.env.example` file to `.env` and fill in database credentials if needed. Defaults do not need to be modified for sqlite database.

#### Migrate

This will create the necessary tables, default records, and superuser account in the database

```
python manage.py migrate
```

## VS Code Setup

#### Install extensions

[Django Template](https://marketplace.visualstudio.com/items?itemName=bibhasdn.django-html)

[Python](https://marketplace.visualstudio.com/items?itemName=ms-python.python)

#### Configure python extension

Set python interpreter to point at the pipenv created previously

Set linting->pylint to enabled

Set formatting->provider to black

## Do the thing

Start the development server

```
python manage.py runserver
```

Browse to http://127.0.0.1:8000 and login as `superuser` with the default password `password`

# License

This project is licensed under the MIT License - see [LICENSE.md](LICENSE.md) for details
